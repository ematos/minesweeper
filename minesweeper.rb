require 'set'
require_relative 'config'
require_relative 'utils'


class Cell
    attr_reader :has_bomb, :has_flag, :is_revealed, :bombs_around_count

    def initialize()
        @has_flag = false
        @has_bomb = false
        @is_revealed = false
        @bombs_around_count = 0
    end

    def has_flag?
        @has_flag
    end

    def has_bomb?
        @has_bomb
    end

    def put_bomb!
        if @has_bomb
            false
        else
            @has_bomb = true
        end
    end

    def toggle_flag!
        return false if @is_revealed
        @has_flag = !@has_flag
    end

    def is_revealed?
        @is_revealed
    end

    def reveal!
        if @is_revealed or has_flag
            false
        else
            @is_revealed = true
        end
    end

    def incr_bombs_around!
        @bombs_around_count += 1
    end

    def to_s
        return CellOutput[:flag] if !is_revealed? and has_flag?
        return CellOutput[:unrevealed] unless is_revealed?

        if has_bomb?
            CellOutput[:bomb]
        elsif @bombs_around_count == 0
            CellOutput[:empty]
        else
            @bombs_around_count.to_s
        end
    end
end


class Board
    attr_reader :rows, :cols, :bombs, :board, :game_over

    @@MAX_ROWS = 25
    @@MAX_COLS = 25

    def initialize(rows, cols, bombs)
        validate_params(rows, cols, bombs)

        @rows = rows
        @cols = cols
        @bombs = bombs

        @revealed_cells_count = 0
        @coords_checked = Set.new
        @game_over = false

        make_board!
    end

    def reveal!(address)
        return false if game_over?

        x, y = excel_to_xy(address)
        return false unless coord_exists? x, y

        @coords_checked << [x, y]

        cell = @board[x][y]
        if cell != nil and cell.reveal!
            @revealed_cells_count += 1
            @revealed_bomb = true if cell.has_bomb?

            if cell.bombs_around_count == 0 and !cell.has_bomb?
                # reveal neighbors
                get_neighbors(x, y).reject do |coord, cell|
                    @coords_checked.include?(coord) or cell.has_bomb?
                end.each do |coord, cell|
                    unless cell.has_bomb?
                        reveal!(xy_to_excel(coord[0], coord[1]))
                    end
                end
            end

            @coords_checked.clear
            true
        else
            @coords_checked.clear
            false
        end
    end

    def toggle_flag!(address)
        return false if @game_over
        x, y = excel_to_xy(address)
        @board[x][y].toggle_flag!
    end

    def won?
        game_over? and !@revealed_bomb
    end

    def to_a
        @board
    end

    def game_over?
        no_plays_left = @revealed_cells_count == (@rows * @cols) - @bombs
        no_plays_left or @revealed_bomb
    end

    def self.MAX_ROWS
        @@MAX_ROWS
    end

    def self.MAX_COLS
        @@MAX_COLS
    end

    private

    attr_accessor :revealed_cells_count, :coords_checked, :revealed_bomb

    def coord_exists?(x, y)
        x < @board.count and y < @board[0].count
    end

    def validate_params(rows, cols, bombs)
        if rows > @@MAX_ROWS
            raise "O número máximo de linhas foi excedido. #{rows} > #{@@MAX_ROWS}"
        end

        if cols > @@MAX_COLS
            raise "O número máximo de colunas foi excedido. #{cols} > #{@@MAX_COLS}"
        end

        if bombs > cols * rows
            raise "O máximo de bombas para um tabuleiro #{rows}×#{cols} são #{rows * cols}"
        end
    end

    def make_board!
        @board = Array.new(@rows)

        @rows.times.each_with_index do |item, i|
            row = Array.new(@cols)
            row.each_with_index { |item, j| row[j] = Cell.new }
            @board[i] = row
        end

        fill_with_bombs!        
    end

    def fill_with_bombs!
        bombs_put = 0
        until bombs_put == @bombs
            x, y = rand(@rows), rand(@cols)
            if @board[x][y].put_bomb!
                bombs_put += 1

                get_neighbors(x, y).each { |coord, cell| cell.incr_bombs_around! }
            end
        end
    end

    def get_neighbors(x, y)
        neighbors = Hash.new

        get_neighbors_coords(x, y).each do |coord|
            neighbor_x, neighbor_y = coord
            neighbors[[neighbor_x, neighbor_y]] = @board[neighbor_x][neighbor_y]
        end

        neighbors
    end

    def get_neighbors_coords(x, y)
        neighbors_coords = []

        -1.upto(1).each do |x_incr|
            x_coord = x + x_incr
            next unless x_coord.between?(0, @rows-1)

            -1.upto(1).each do |y_incr|
                y_coord = y + y_incr
                next unless y_coord.between?(0, @cols-1)
                next if x_incr == 0 and y_incr == 0

                neighbors_coords << [x_coord, y_coord]
            end
        end

        neighbors_coords
    end
end
