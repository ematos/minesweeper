require 'set'
require_relative 'minesweeper'
require_relative 'formatter'


def ask_for_game_setup
    puts ''
    print "Quantas linhas (#{Board.MAX_ROWS} máx): "
    rows = gets.to_i
    print "Quantas colunas (#{Board.MAX_COLS} máx): "
    cols = gets.to_i
    print "Quantas bombas (#{rows * cols} máx): "
    bombs = gets.to_i
    puts ''

    [rows, cols, bombs]
end


def ask_for_address
    puts ''
    print '~> Digite um endereço (Ex.: C4): '
    address = gets.chomp
    puts ''
    address
end


def parse_address(address)
    matches = /^(?<flag>f )?(?<address>.*)$/.match(address.downcase)
    [matches[:address], matches[:flag] != nil]
end

def yesno(msg)
    answer = nil
    until answer != nil
        print "#{msg} (s/n): "
        answer = {'s' => true, 'n' => false}[gets.chomp.downcase]
    end

    answer
end


while yesno 'Continuar jogando?'
    board = Board.new(*ask_for_game_setup)
    puts Formatter::pretty(board)

    until board.game_over?
        address, is_flag = parse_address(ask_for_address)
        is_flag ? board.toggle_flag!(address) : board.reveal!(address)
        puts Formatter::pretty(board)
    end

    puts "\n"
    puts board.won? ? 'Cê é o bixão mesmo, hein doido!' : 'Achou que ia ganhar?? Achou errado, otário!'
    puts "\n\n"

    if yesno 'Ver gabarito?'
        puts Formatter::pretty(board, true)
        puts ''
    end
end

puts ''
puts "Obrigado por jogar! 🙂"
