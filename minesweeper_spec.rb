require 'test/unit'
require_relative 'minesweeper'
require_relative 'config'


class CellTest < Test::Unit::TestCase
    def test_adds_bomb
        cell = Cell.new
        assert_false cell.has_bomb?
        
        cell.put_bomb!
        assert_true cell.has_bomb?
    end

    def test_put_bomb_returns_false_if_cell_already_has_bomb
        cell = Cell.new

        assert_true cell.put_bomb!
        assert_false cell.put_bomb!
    end

    def test_toggle_flag
        cell = Cell.new
        assert_false cell.has_flag?

        cell.toggle_flag!
        assert_true cell.has_flag?

        cell.toggle_flag!
        assert_false cell.has_flag?
    end

    def test_revealed
        cell = Cell.new
        assert_false cell.is_revealed?

        cell.reveal!
        assert_true cell.is_revealed?
    end

    def test_doesnt_reveal_if_flagged
        cell = Cell.new

        cell.toggle_flag!
        cell.reveal!
        assert_false cell.is_revealed?

        cell.toggle_flag!
        cell.reveal!
        assert_true cell.is_revealed?
    end

    def test_doesnt_flag_if_revealed
        cell = Cell.new

        cell.reveal!
        assert_false cell.toggle_flag!
        assert_false cell.has_flag?
    end

    def test_reveal_returns_false_if_cell_already_revealed
        cell = Cell.new

        assert_true cell.reveal!
        assert_false cell.reveal!
    end

    def test_outputs_black_square_if_cell_is_not_revealed
        cell = Cell.new
        assert_equal CellOutput[:unrevealed], cell.to_s

        cell.incr_bombs_around!
        assert_equal CellOutput[:unrevealed], cell.to_s

        cell.put_bomb!
        assert_equal CellOutput[:unrevealed], cell.to_s

        cell.reveal!
        assert_not_equal CellOutput[:unrevealed], cell.to_s
    end

    def test_bombs_around
        cell = Cell.new
        assert_equal 0, cell.bombs_around_count

        cell.incr_bombs_around!
        assert_equal 1, cell.bombs_around_count
    end

    def test_outputs_bombs_around
        cell = Cell.new
        cell.incr_bombs_around!
        cell.reveal!
        
        assert_equal '1', cell.to_s
    end

    def test_outputs_bomb_if_has_bomb
        cell = Cell.new
        cell.put_bomb!
        cell.incr_bombs_around!
        cell.reveal!

        assert_equal CellOutput[:bomb], cell.to_s
    end

    def test_output_flag_if_call_is_flagged
        cell = Cell.new
        cell.toggle_flag!

        assert_equal CellOutput[:flag], cell.to_s
    end
end


class BoardTest < Test::Unit::TestCase
    def test_raise_if_exceeds_max_cols_or_rows
        assert_raise RuntimeError do
            Board.new(Board.MAX_ROWS + 1, 1, 1)
        end

        assert_raise RuntimeError do
            Board.new(1, Board.MAX_COLS + 1, 1)
        end
    end

    def test_raises_if_bomb_count_exceed_number_of_cells
        assert_raise RuntimeError do
            Board.new(3, 3, 3 * 3 + 1)
        end
    end

    def test_make_board_of_a_given_size
        board = Board.new(3, 7, 2)
        raw_board = board.to_a

        assert_kind_of Array, raw_board
        assert_equal 3, raw_board.count # 3 rows

        raw_board.each do |row|
            assert_equal 7, row.count # 7 cols

            # Every cell is a Cell instance
            row.each { |cell| assert_kind_of Cell, cell }
        end
    end

    def test_fills_with_bombs
        board = Board.new(3, 3, 4)

        total_bombs = 0
        board.to_a.each do |row|
            total_bombs += row.select { |cell| cell.has_bomb? }.count
        end
    end

    def test_increments_bomb_counter
        board1 = Board.new(2, 2, 1)
        board2 = Board.new(2, 2, 2)
        board3 = Board.new(2, 2, 3)

        board1.to_a.each do |row|
            row.each do |cell|
                unless cell.has_bomb?
                    assert_equal 1, cell.bombs_around_count
                end
            end
        end

        board2.to_a.each do |row|
            row.each do |cell|
                unless cell.has_bomb?
                    assert_equal 2, cell.bombs_around_count
                end
            end
        end

        board3.to_a.each do |row|
            row.each do |cell|
                unless cell.has_bomb?
                    assert_equal 3, cell.bombs_around_count
                end
            end
        end
    end

    def test_reveal_cell
        board = Board.new(2, 2, 1)

        assert_true board.reveal!('B1')
        assert_true board.to_a[0][1].is_revealed
        assert_false board.reveal!('B1')
    end

    def test_returns_false_try_to_reveal_unexisting_cell
        board = Board.new(2, 2, 1)
        assert_false board.reveal!('Z10')
    end

    def test_toggle_flag
        board = Board.new(2, 2, 1)

        board.toggle_flag!('B2')
        assert_true board.to_a[1][1].has_flag

        board.toggle_flag!('B2')
        assert_false board.to_a[1][1].has_flag
    end

    def test_player_loses
        board = Board.new(2, 1, 1)
        address_with_bomb = board.to_a[0][0].has_bomb? ? 'A1' : 'A2' 

        board.reveal!(address_with_bomb)
        assert_false board.won?
    end

    def test_player_wins
        board = Board.new(2, 1, 1)
        address_without_bomb = board.to_a[0][0].has_bomb? ? 'A2' : 'A1' 

        board.reveal!(address_without_bomb)
        assert_true board.won?
    end

    def test_reveals_neighbor_empty_cells_if_empty_cell_is_revealed
        board = Board.new(5, 5, 0)

        board.reveal!('C3')

        board.to_a.each do |row|
            row.each do |cell|
                assert_true cell.is_revealed?
            end
        end

    end

    def test_doesnt_reveal_neighbor_if_cell_is_not_empty
        board = Board.new(1, 25, 1)

        bombless_cell_x = 0
        bombless_cell_y = board.to_a[0].find_index { |cell| !cell.has_bomb? and cell.bombs_around_count > 0 }

        address = "#{INDEX_TO_LETTER[bombless_cell_y]}1"
        board.reveal!(address)

        board.to_a[0].each_with_index do |cell, y|
            assert_equal(cell.is_revealed?,  y == bombless_cell_y)
        end
    end
end
