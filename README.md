# Minesweeper

Jogo campo minado escrito em Ruby.

## Inicializar

```sh
rake play
```

## Como jogar

Primeiro escolher as configurações iniciais do jogo (quantidade de linhas, colunas e bombas):

![](https://user-images.githubusercontent.com/483681/39531122-de8924e0-4e00-11e8-9b4d-36776c765573.png)

Durante o jogo, deve-se passar o endereço da célula que deseja revelar. Os endereços são no estilo Excel. Ex.: `C5`, ou `A10`... Tanto faz usar letra maiúscula ou minúscula.

![](https://user-images.githubusercontent.com/483681/39530297-1171bc48-4dff-11e8-867a-d117e21e1fdc.png)

Se o endereço não existir ou já tiver sido revelado, o movimento será ignorado.

Se uma célula vazia for revelada, isto é, uma célula sem bombas em volta, todas as células em seu entorno também serão reveladas.

![](https://user-images.githubusercontent.com/483681/39530214-df3384b4-4dfe-11e8-834f-f374fc858695.png)

Para adicionar uma bandeira, antes do endereço da célula é necessário digitar `F`. Ex.: `F G2`.

![](https://user-images.githubusercontent.com/483681/39530388-3c0bd74a-4dff-11e8-8915-b1216c1dfbe0.png)

Se uma célula com bomba for clicada, o jogo termina e o jogador perde.

![](https://user-images.githubusercontent.com/483681/39530453-620cdba6-4dff-11e8-872b-39f96d5218cd.png)

Para vencer o jogo, é necessário que todas as células sem bomba sejam reveladas.

![](https://user-images.githubusercontent.com/483681/39530614-beb10ee0-4dff-11e8-8d98-3ccdbdc6e283.png)

Ao finalizar o jogo, é possível optar por ver o gabarito.

![](https://user-images.githubusercontent.com/483681/39530504-7ce256e0-4dff-11e8-8280-c106edbe5ee2.png)

## Testes

```sh
rake test
```
