require 'test/unit'
require_relative 'utils'


class ExcelToXyTest < Test::Unit::TestCase
    def test_convert_to_xy
        assert_equal([0, 0], excel_to_xy('A1'))
        assert_equal([1, 1], excel_to_xy('B2'))
        assert_equal([12, 16], excel_to_xy('Q13'))
    end

    def test_works_up_to_column_1000
        assert_equal([0, 1000], excel_to_xy('ALM1'))
        assert_raise(RuntimeError) { excel_to_xy('ALN1') }
    end

    def test_ignores_case
        assert_equal([11, 27], excel_to_xy('aB12'))
    end
end


class XyToExcelTest < Test::Unit::TestCase
    def test_convert_to_excel
        assert_equal('A1', xy_to_excel(0, 0))
        assert_equal('B2', xy_to_excel(1, 1))
        assert_equal('Q13', xy_to_excel(12, 16))
    end

    def test_works_up_to_column_1000
        assert_equal('ALM1', xy_to_excel(0, 1000))
        assert_raise(RuntimeError) { xy_to_excel(0, 1001) }
    end
end