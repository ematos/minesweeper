require_relative 'utils'
require_relative 'minesweeper'


module Formatter
    def self.pretty(board, x_ray = false)
        if x_ray
            board = deep_clone(board)
            board.to_a.each do |row|
                row.each do |cell|
                    if cell.has_flag?
                        cell.toggle_flag!
                    end
                    cell.reveal!
                end
            end
        end

        output = "\n"
        coord_header = '     ' + board.cols.times.map { |i| "#{INDEX_TO_LETTER[i]}" }.join("   ") + "  \n"
        top = bottom = middle = "   ┼" + ("───┼" * board.cols)

        output += coord_header + top + "\n"

        board.to_a.each_with_index do |row, row_index|
            output += "#{row_index + 1}".rjust(Board.MAX_COLS.to_s.length + 1, ' ')
            row.each do |cell|
                output += "│ #{cell.to_s} "
            end
            output += "│ #{row_index + 1}\n#{middle}\n"
        end

        output + coord_header
    end

    # This function exists just because the requirements say
    # we need two output options, but does not mention how they
    # will be used.
    def self.silly(board, x_ray = false)
        board.to_a.inspect
    end
end

def deep_clone(obj)
    # I know, it's dumb to clone like this, and it probably
    # should not be used on production.
    # But c'mon, it is a simple and fast enough game,
    # so I'm not really concerned with this decision.
    Marshal.load(Marshal.dump(obj))
end
