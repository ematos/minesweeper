CellOutput = {
    :unrevealed => '■',
    :flag => '⚑',
    :bomb => '💣',
    :empty => ' ',
}
