INDEX_TO_LETTER = Hash.new { |hash,key| hash[key] = hash[key - 1].next }.merge({0 => "a"})
INDEX_TO_LETTER[1000] # Force creating hashes up to 1000 columns

LETTER_TO_INDEX = INDEX_TO_LETTER.invert


def excel_to_xy(address)
    matches = /^(?<column>[a-z]+)(?<row>[0-9]+)$/.match(address.downcase)
    row, column = [matches[:row], matches[:column]]

    raise 'Column value exceeded' unless LETTER_TO_INDEX.key?(column)

    [row.to_i - 1, LETTER_TO_INDEX[column]]
end

def xy_to_excel(x, y)
    raise 'Column value exceeded' unless INDEX_TO_LETTER.key?(y)
    "#{INDEX_TO_LETTER[y]}#{x + 1}".upcase
end
